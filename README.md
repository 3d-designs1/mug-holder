This is meant to replace the default Mug-Bed on the Philips Saeco Coffee machine to fit larger mugs.

This part still collects excess water and coffee while being around 1.5cm lower. I printed it nearly solid to make it as watertight as possible. PLA should be fine, but more temperature resistant materials may be preferable.

Happy printing!
